package net.techu.api.controllers;

import net.techu.api.Producto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@RestController
public class ProductosController {

    private static Map<String, Producto> listaProductos = new HashMap<>();

    /*Inicializar lista de productos*/
    static {
        Producto pr1 = new Producto();
        pr1.setId("PR1");
        pr1.setNombre("PRODUCTO 1");
        listaProductos.put(pr1.getId(), pr1);

        Producto pr2 = new Producto();
        pr2.setId("PR2");
        pr2.setNombre("PRODUCTO 2");
        listaProductos.put(pr2.getId(), pr2);
    }

    @GetMapping(value = "/productos")
    //@RequestMapping(value = "/productos", method = RequestMethod.GET)
    public Map<String, Producto> getProductos()
    {
        return listaProductos;
    }

    @Autowired
    ResourceLoader cargador;

    @GetMapping(value = "/v2/productos")
    //@RequestMapping(value = "/productos", method = RequestMethod.GET)
    public String getProductosDeFichero()
    {
        String nombreFichero = "classpath:static/productos.json";
        Resource recursoFichero = cargador.getResource(nombreFichero);
        String productos = "";

        try {
            InputStream streamDatos = recursoFichero.getInputStream();
            byte[] bytesDocumento = streamDatos.readAllBytes();
            productos = new String(bytesDocumento, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return productos;
    }

    @GetMapping(value = "/v3/productos")
    //@RequestMapping(value = "/productos", method = RequestMethod.GET)
    public String getProductosDeOtraAPI()
    {
        String urlAPI = "https://gist.githubusercontent.com/keeguon/2310008/raw/bdc2ce1c1e3f28f9cab5b4393c7549f38361be4e/countries.json";
        String productos = "";
        try {
            URL url = new URL(urlAPI);
            HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
            conexion.setRequestMethod("GET");
            conexion.setRequestProperty("User-Agent", "MiApp");
            conexion.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            InputStreamReader entrada = new InputStreamReader(conexion.getInputStream());
            int respuesta = conexion.getResponseCode();
            System.out.println(new StringBuilder().append("Respuesta : ").append(respuesta));
            if (respuesta == HttpURLConnection.HTTP_OK) {
                BufferedReader lector = new BufferedReader(entrada);
                String lineaLeida;
                StringBuffer resultado = new StringBuffer();
                while ((lineaLeida = lector.readLine()) != null) {
                    resultado.append(lineaLeida);
                }
                lector.close();
                productos = resultado.toString();
            }
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return productos;
    }

    @RequestMapping(value = "/productos/{id}", method = RequestMethod.GET)
    public ResponseEntity<Object> getProducto(@PathVariable("id") String id){
        if (listaProductos.containsKey(id.toUpperCase())){
            return new ResponseEntity<>(listaProductos.get(id),HttpStatus.OK);
        }
        return new ResponseEntity<>(String.format("No he encontrado el producto %s", id), HttpStatus.NOT_FOUND);
    }

    //@RequestMapping("/productos")
    public String productos(){
        return "Lista de productos";
    }

    //@RequestMapping("/productos")
    public String productos(@RequestParam(value = "id", required = true) String id) {
        return String.format("Info del producto %s", id);
    }

    @PostMapping("/productos")
    public ResponseEntity<Object> addProducto(@RequestBody Producto productoNuevo){
        if (listaProductos.containsKey(productoNuevo.getId().toUpperCase())){
            return new ResponseEntity<>("Producto ya existente", HttpStatus.CONFLICT);
        }

        listaProductos.put(productoNuevo.getId(), productoNuevo);
        return new ResponseEntity<>(String.format("Producto dado de alta"), HttpStatus.CREATED);
    }

    @PutMapping("/productos/{id}")
    public ResponseEntity<Object> updateProducto(@PathVariable("id") String id, @RequestBody Producto productoCambiado){
        if (!id.toUpperCase().equals(productoCambiado.getId().toUpperCase())){
            return new ResponseEntity<>(String.format("Datos incorrectos", id), HttpStatus.CONFLICT);
        }
        if (listaProductos.containsKey(id.toUpperCase())){
            listaProductos.put(id, productoCambiado);
            return new ResponseEntity<>(String.format("Producto %s actualizado", id), HttpStatus.OK);
        }
        listaProductos.put(id, productoCambiado);
        return new ResponseEntity<>(String.format("Producto %s creado", id), HttpStatus.CREATED);
    }

    @DeleteMapping("/productos/{id}")
    public ResponseEntity<Object> deleteProducto(@PathVariable("id") String id){
        if (listaProductos.containsKey(id.toUpperCase())){
            listaProductos.remove(id);
            return new ResponseEntity<>(String.format("Producto %s eliminado", id), HttpStatus.OK);
        }
        return new ResponseEntity<>(String.format("Producto %s no encontrado", id), HttpStatus.NOT_FOUND);
    }
}
