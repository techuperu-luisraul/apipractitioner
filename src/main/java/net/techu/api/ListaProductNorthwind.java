package net.techu.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ListaProductNorthwind implements Serializable {
    @JsonProperty("value")
    private List<ProductNorthwind> productos;

    public ListaProductNorthwind() {
        productos = new ArrayList();
    }

    public List<ProductNorthwind> getProductos() {
        return productos;
    }
}
